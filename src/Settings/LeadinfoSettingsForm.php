<?php

namespace Drupal\leadinfo\Settings;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class LeadinfoSettingsForm extends ConfigFormBase {

    public function getFormId() {
        return 'leadinfo_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state){
        $form = parent::buildForm($form, $form_state);
        $leadinfo_id = $this->config('leadinfo.settings')->get('leadinfo_id');

        $form['leadinfo_id'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Leadinfo id'),
            '#default_value' => $leadinfo_id,
            '#description' => $this->t('<br>Leadinfo enables you to see real-time which companies are visiting your website and what pages they’re viewing to increase your sales.<br>
             <strong>Plugin Configuration</strong><br>
             If you don’t have an account yet get one free at <a href="https://www.leadinfo.com/en/?utm_source=drupal" target="_blank">leadinfo.com</a><br>
             <strong>Configuration options</strong>
             <ol>
                <li>Visit your Leadinfo Portal, go to "Settings" and select under ‘Trackers’ the URL of the website you wish to track.</li>
                <li>Copy your Leadinfo Site ID, starting with LI-xxx.</li>
                <li>Return to Drupal and go to Configuration > Leadinfo settings to paste your Leadinfo Site ID.</li>
            </ol>'),
        ];
        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state) {
        global $user;

        $leadinfo_id = $form_state->getValue('leadinfo_id');

        $matched = preg_match('/^(LI\-)([0-9A-Z]+)$/', $leadinfo_id);

        if($matched){
            $this->config('leadinfo.settings')->set('leadinfo_id', $leadinfo_id)->save();
            parent::submitForm($form, $form_state);
            drupal_flush_all_caches();
        }else{
            drupal_set_message(t('Incorrect Leadinfo Site ID, please try again.'), 'error');
        }
    }

    protected function getEditableConfigNames() {
        return [
            'leadinfo.settings',
        ];
    }

}